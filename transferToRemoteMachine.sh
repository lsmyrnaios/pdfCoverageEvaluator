# We can only connect to Impala from authorized IPs, so we have to transfer this python-project-directory to such a machine.

remote_user_with_ip='<USER>@<IP>' # Fill this information as needed.

echo -e "Removing remote directory.."
ssh -t ${remote_user_with_ip} "rm -rf pdfCoverageEvaluator"

cd ../
scp -r -C pdfCoverageEvaluator ${remote_user_with_ip}:.

ssh -t ${remote_user_with_ip} "python3 --version; sudo apt install -y python3-pip; sudo pip3 install -r ./pdfCoverageEvaluator/requirements.txt"
