# We can only connect to Impala from authorized IPs, so we have to transfer this python-project-directory to such a machine.

remote_user_with_ip='<USER>@<IP>' # Fill this information as needed.

echo -e "Removing remote directory.."
ssh -t ${remote_user_with_ip} "rm -rf pdfCoverageEvaluator"

cd ../
scp -r -C pdfCoverageEvaluator ${remote_user_with_ip}:.

input_file_path='pids_to_check.json'
max_num_to_process=-1 # -1 == All records
#previous_results_file_path='previous_results.json' # Uncomment this, if you want to exclude the pids from the input which have already been processed and gave a result in a previous run.

ssh -t ${remote_user_with_ip} "python3 --version; sudo apt install -y python3 python3-pip; sudo pip3 install --upgrade pip; cd pdfCoverageEvaluator; sudo pip3 install -r requirements.txt; (screen -d -m python3 pdfCoverageEvaluator.py ${input_file_path} ${max_num_to_process} ${previous_results_file_path} && screen -r); tail -n 100 pdfCoverageEvaluator.log"

# In case the program fails and the screen terminates without logs, use the following command to debug (without the screen):
#ssh -t ${remote_user_with_ip} "python3 --version; sudo apt install -y python3 python3-pip; sudo pip3 install --upgrade pip; cd pdfCoverageEvaluator; sudo pip3 install -r requirements.txt; python3 pdfCoverageEvaluator.py ${input_file_path} ${max_num_to_process}" # ${previous_results_file_path}"
